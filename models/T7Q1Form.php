<?php

namespace app\models;

use yii\base\Model;

/**
 * T7Q1Form is the model behind the test form.
 *
 * @property string $phoneNumber.
 * @property string $phoneFormat.
 *
 */
class T7Q1Form extends Model
{
    public $phoneNumber;
    public $phoneFormat;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['phoneNumber','phoneFormat'], 'trim'],
            ['phoneFormat', 'default'],
            ['phoneNumber', 'number'],
            ['phoneNumber', 'string', 'length' => 11],
            ['phoneNumber', 'required'],
        ];
    }
}
