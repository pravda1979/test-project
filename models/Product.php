<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Модель продукции. Используется для получения данных, их фильтрации и сортировки.
 *
 * @property int $id.
 * @property int $categoryId.
 * @property string $categoryName.
 * @property int $price.
 * @property int $hidden.
 */
class Product extends Model
{
    public $id;
    public $categoryId;
    public $categoryName;
    public $price;
    public $hidden;

    private $_filtered = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id', 'categoryId', 'categoryName', 'price', 'hidden'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoryId' => 'ID категории',
            'categoryName' => 'Категория',
            'price' => 'Цена',
            'hidden' => 'Скрыто'
        ];
    }

    /**
     * Возвращает ArrayDataProvider, содержащий отфильтрованный и отсортированный результат, содержащий список продукции с их категориями
     * $params - массив Yii::$app->request->get(), переданныей в actionProducts().
     * Они будут загружены и провалидированы.
     * Если валидация пройдет успешно, свойству _filtered будет присвоено true для фильтрации результата
     * Если нет, то результат фильтроваться не будет.
     * @param $params
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        if ($this->load($params) && $this->validate()) {
            $this->_filtered = true;
        }

        return new ArrayDataProvider([
            'allModels' => $this->getData(),
            'key' => 'id',
            'modelClass' => Product::class,
            'sort' => [
                'attributes' => ['id', 'categoryId', 'categoryName', 'price', 'hidden'],
                'defaultOrder' => ['id' => SORT_ASC],
                'enableMultiSort' => true
            ],
        ]);
    }

    /**
     * Возвращает массив, содержащий отфильтрованный и отсортированный список продукции с их категориями
     * @return array
     */
    protected function getData()
    {
        $categories = [];
        $data = [];

        /**
         * Загружаем содержимое xml файлов
         */
        $prodObj = simplexml_load_file(Yii::getAlias('@app/web/files/products.xml'));
        $catObj = simplexml_load_file(Yii::getAlias('@app/web/files/categories.xml'));

        /**
         * Формируем справочник категорий
         */
        foreach ($catObj as $obj) {
            $categories[(int)$obj->id] = mb_convert_encoding($obj->name, 'UTF-8');
        }

        /**
         * Формируем строки данных
         */
        foreach ($prodObj as $product) {
            $data[] = [
                'id' => (int)$product->id,
                'categoryId' => (int)$product->categoryId,
                'categoryName' => ArrayHelper::getValue($categories, (int)$product->categoryId),
                'price' => (int)$product->price,
                'hidden' => (int)$product->hidden,
            ];
        }

        /**
         * Фильтруем записи, если необходимо
         */
        if ($this->_filtered) {
            $data = array_filter($data, function ($value) {
                $conditions = [true];
                if (!empty($this->id))
                    $conditions[] = strpos($value['id'], $this->id) !== false;
                if (!empty($this->categoryId))
                    $conditions[] = stripos($value['categoryId'], $this->categoryId) !== false;
                if (!empty($this->categoryName))
                    $conditions[] = stripos($value['categoryName'], $this->categoryName) !== false;
                if (!empty($this->price))
                    $conditions[] = strpos($value['price'], $this->price) !== false;
                if (!is_null($this->hidden) && $this->hidden !== '')
                    $conditions[] = strpos($value['hidden'], $this->hidden) !== false;
                return array_product($conditions);
            });
        }

        return $data;
    }
}
