<?php

namespace app\controllers;

use app\models\Product;
use Yii;
use app\models\T7Q1Form;
use yii\web\Controller;

class TestController extends Controller
{
    /**
     * @return string
     */
    public function actionT7Q1()
    {
        $model = new T7Q1Form();

        if ($model->load(Yii::$app->request->post()))
            $model->validate();

        return $this->render('t7q1', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionProducts()
    {
        $searchModel = new Product();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('products', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionNullDisplay()
    {
        $searchModel = new Product();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('null_display', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionTruncateWords()
    {
        $sourceText = '';
        for ($i = 1; $i <= 30; $i++) {
            $sourceText .= " word$i";
        }

        return $this->render('11_1', [
            'sourceText' => $sourceText,
        ]);
    }

    /**
     * @return string
     */
    public function actionIdToCamel()
    {
        $sourceText = 'created_at';

        return $this->render('11_2', [
            'sourceText' => $sourceText,
        ]);
    }

    /**
     * @return string
     */
    public function actionSlug()
    {
        $sourceText = 'Купи слона';

        return $this->render('11_3', [
            'sourceText' => $sourceText,
        ]);
    }

    /**
     * @return string
     */
    public function actionEssay()
    {
        return $this->render('essay');
    }

    /**
     * @return string
     */
    public function actionTinymce()
    {
        return $this->render('tinymce');
    }
}
