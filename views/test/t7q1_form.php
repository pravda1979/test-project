<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\T7Q1Form */

?>

<div class="test-t7q1-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'phoneNumber')->textInput(['autofocus' => true])->hint('Номер телефона в виде 79891112233 (11 цифр)') ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'phoneFormat')->textInput()->hint('По умолчанию формат равен "+$1($2)$3-$4-$5", что соответствует такому результату: +7(989)111-22-33') ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
