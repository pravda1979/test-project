<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $sourceText string */

$this->title = 'Задание 11. Вопрос 1';
?>

<div class="test-t10q1">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <div>
        Вам нужно обрезать строку, по определенному количеству слов. Что бы из 30 слов, выводилось 12. Приложите ссылку
        на код.
    </div>
    <br>
    <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/ae0ae34c698be4a570c54aa7bf7627054a564543', ['class' => 'btn btn-default', 'style' => 'margin-top:5px;']) ?>
    <hr>

    Используем хелпер: <code>StringHelper::truncateWords($sourceText, 12)</code>
    <hr>

    <h4>Исходный текст:</h4>
    <?= Html::tag('code',
        $sourceText
    ); ?>

    <h4>Обрезанный текст:</h4>
    <?= Html::tag('code',
        $this->render("11_1_source", [
            'sourceText' => $sourceText
        ])
    ); ?>
</div>