<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $sourceText string */

$this->title = 'Задание 11. Вопрос 2';
?>

<div class="test-t11q2">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <div>
        Вам нужно преобразовать строку из created_at в CreatedAt. Приложите ссылку на код.
    </div>
    <br>
    <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/d38840db35dcfaded02d4d4bf1b9d22d19f5e9b3', ['class' => 'btn btn-default', 'style' => 'margin-top:5px;']) ?>
    <hr>

    Используем хелпер: <code>Inflector::id2camel($sourceText, '_')</code>
    <hr>

    <h4>Исходный текст:</h4>
    <?= Html::tag('code',
        $sourceText
    ); ?>

    <h4>Преобразованный текст:</h4>
    <?= Html::tag('code',
        $this->render("11_2_source", [
            'sourceText' => $sourceText
        ])
    ); ?>
</div>