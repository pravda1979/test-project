<?php

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $sourceText string */

echo StringHelper::truncateWords($sourceText, 12);
