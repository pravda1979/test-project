<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\T7Q1Form */

$this->title = 'Задание 7. Вопрос 1';
?>

<div class="test-t7q1">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <div>
        Напишите свой formatter телефонного номера, формат/маска задается опционально. В базе лежит числовое
        представление номера.
    </div>
    <hr>
    <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/68045839b32134fcb3148110ec1a4e90432ed371', ['class'=>'btn btn-default']) ?>

    <div class="jumbotron">
        <?= Html::tag('code', $model->phoneNumber ?
            'Результат: ' . Yii::$app->formatter->asPhone($model->phoneNumber, $model->phoneFormat) :
            'Введите номер телефона') ?>
    </div>

    <?= $this->render("t7q1_form", ['model' => $model]) ?>

</div>
