<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $sourceText string */

$this->title = 'Задание 11. Вопрос 3';
?>

<div class="test-t11q3">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <div>
        Вам нужно строку "Купи слона" преобразовать в "Kupi slona". Приложите ссылку на код.
    </div>
    <br>
    <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/717fb81c0ea0a701ec1ba1ec3245fd64af358fa6', ['class' => 'btn btn-default', 'style' => 'margin-top:5px;']) ?>
    <hr>

    Используем хелпер: <code>Inflector::slug($sourceText, ' ', false)</code>
    <hr>

    <h4>Исходный текст:</h4>
    <?= Html::tag('code',
        $sourceText
    ); ?>

    <h4>Преобразованный текст:</h4>
    <?= Html::tag('code',
        $this->render("11_3_source", [
            'sourceText' => $sourceText
        ])
    ); ?>
</div>