<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'TinyMCE editor';
$rr = new \pravda1979\tinymce\Asset();
?>

<div class="test-tinymce">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <div>
        Написать компонент визуального редактора ( на выбор ) , с возможностью его настройки и установки через composer.
        Зарегистрировать на https://packagist.org. Приложить ссылку.
    </div>
    <br>
    <?= Html::a('GitHub', 'https://github.com/pravda1979/yii2-tinymce', ['class' => 'btn btn-default']) ?>
    <?= Html::a('Packagist', 'https://packagist.org/packages/pravda1979/yii2-tinymce', ['class' => 'btn btn-success']) ?>
    <hr>

    <?= \pravda1979\tinymce\TinyMCE::widget([
        'name' => 'test-tinymce',
        'value' => 'test TinyMCE editor',
        'language' => 'ru',
    ]); ?>
</div>