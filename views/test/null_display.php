<?php

use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\Product */
/* @var $dataProvider \yii\data\ArrayDataProvider */

$this->title = 'Задание 10. Вопрос 1';
?>

<div class="test-t7q1">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <div>
        Есть проект который разрабатывался длительное время, с большим количеством GridView. Теперь
        клиент захотел что бы везде в списках (GridView) где выводиться "(не задано)" если column
        возвращает null , выводилась фраза "[нет данных]". Что вы сделаете ? Укажите ссылку на
        решение.
    </div>
    <br>
    <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/7c536d76850e356e0eddb43cbf3840e7f38ff9c9', ['class' => 'btn btn-default', 'style' => 'margin-top:5px;']) ?>
    <hr>

    <?php Pjax::begin(); ?>

    <?= $this->render("products_search", ['searchModel' => $searchModel]) ?>

    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'categoryId',
                'value' => function () {
                    return null;
                },
            ],
            'categoryName',
            'price',
            'hidden:boolean'
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
