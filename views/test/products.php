<?php

use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\Product */
/* @var $dataProvider \yii\data\ArrayDataProvider */

$this->title = 'Задание 7. Вопрос 2';
?>

<div class="test-t7q1">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <div>
        Вывести список продуктов и связанных с ними категорий используя GridView, с возможностью
        сортировать, фильтровать. Из xml https://drive.google.com/open?id=1l_idMT9M-UfBD99AHoxRhwADbmJi9oOm.
    </div>
    <?= Html::a('categories.xml', [Yii::getAlias('/files/categories.xml')]) ?>
    <?= Html::a('products.xml', [Yii::getAlias('/files/products.xml')]) ?>
    <br>
    <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/272d97f2c82537a3ca374483268b356f7c29703d', ['class' => 'btn btn-default', 'style' => 'margin-top:5px;']) ?>
    <hr>

    <?php Pjax::begin(); ?>

    <?= $this->render("products_search", ['searchModel' => $searchModel]) ?>

    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
//            'categoryId',
            'categoryName',
            'price',
            'hidden:boolean'
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
