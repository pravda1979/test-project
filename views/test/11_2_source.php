<?php

use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $sourceText string */

echo Inflector::id2camel($sourceText, '_');
