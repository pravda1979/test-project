<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-search">
    <?= Html::a('Фильтр', '#', ['data-toggle' => 'modal', 'data-target' => '#searchModal', 'class' => 'btn btn-primary', 'title' => 'Открыть форму фильтра', 'style' => 'margin:5px;']) ?>
    <?= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-ban-circle']) . ' Сброс', ['/' . Yii::$app->controller->route], ['class' => 'btn btn-warning', 'title' => 'Сбросить фильтр и сортировку данных', 'data-pjax' => 0]) ?>

    <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scroll modal-lg">

            <?php $form = ActiveForm::begin([
                'method' => 'get',
                'action' => ['/' . Yii::$app->requestedRoute],
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-3',
                        'offset' => 'col-sm-offset-3',
                        'wrapper' => 'col-sm-9',
                        'error' => '',
                        'hint' => '',
                        'options' => [
                            'data-pjax' => 1
                        ],
                    ],
                ],
            ]); ?>

            <div class="modal-content">

                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span>
                    </button>
                    <h4 class="modal-title">Поиск продукции</h4>
                </div>

                <div class="modal-body modal-body-scroll">

                    <?= $form->field($searchModel, 'id')->input('number') ?>

                    <?= $form->field($searchModel, 'categoryName')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($searchModel, 'price')->input('number') ?>

                    <?= $form->field($searchModel, 'hidden')->dropDownList(['1' => 'Да', '0' => 'Нет'], ['prompt' => '']) ?>

                </div>

                <div class="modal-footer">
                    <?= Html::submitButton(Html::tag('span', '', ['class' => 'glyphicon glyphicon-search']) . ' Найти', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-ban-circle']) . ' Сброс', ['/' . Yii::$app->controller->route], ['class' => 'btn btn-warning', 'data-pjax' => 0]) ?>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
