<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Задание 7. ОТОБРАЖЕНИЕ ДАННЫХ</h3>
                    </div>
                    <div class="panel-body fixed-height">
                        <h4>Вопрос 1</h4>
                        <p>Напишите свой formatter телефонного номера, формат/маска задается опционально. В базе лежит
                            числовое представление номера.</p>
                        <p>
                            <?= Html::a('Demo', ['/test/t7-q1'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/68045839b32134fcb3148110ec1a4e90432ed371', ['class' => 'btn btn-default']) ?>
                        </p>
                        <hr>
                        <h4>Вопрос 2</h4>
                        <p>Вывести список продуктов и связанных с ними категорий используя GridView, с возможностью
                            сортировать, фильтровать. Из
                            xml https://drive.google.com/open?id=1l_idMT9M-UfBD99AHoxRhwADbmJi9oOm.</p>
                        <p>
                            <?= Html::a('Demo', ['/test/products'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/272d97f2c82537a3ca374483268b356f7c29703d', ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Задание 10. ВИДЖЕТЫ</h3>
                    </div>
                    <div class="panel-body fixed-height">
                        <h4>Вопрос 1</h4>
                        <p>Есть проект который разрабатывался длительное время, с большим количеством GridView. Теперь
                            клиент захотел что бы везде в списках (GridView) где выводиться "(не задано)" если column
                            возвращает null , выводилась фраза "[нет данных]". Что вы сделаете ? Укажите ссылку на
                            решение.</p>
                        <p>
                            <?= Html::a('Demo', ['/test/null-display'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/7c536d76850e356e0eddb43cbf3840e7f38ff9c9', ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Задание 11. ХЕЛПЕРЫ</h3>
                    </div>
                    <div class="panel-body fixed-height">
                        <h4>Вопрос 1</h4>
                        <p>Вам нужно обрезать строку, по определенному количеству слов. Что бы из 30 слов, выводилось
                            12. Приложите ссылку на код.</p>
                        <p>
                            <?= Html::a('Demo', ['/test/truncate-words'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/ae0ae34c698be4a570c54aa7bf7627054a564543', ['class' => 'btn btn-default']) ?>
                        </p>
                        <hr>
                        <h4>Вопрос 2</h4>
                        <p>Вам нужно преобразовать строку из created_at в CreatedAt. Приложите ссылку на код.</p>
                        <p>
                            <?= Html::a('Demo', ['/test/id-to-camel'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/d38840db35dcfaded02d4d4bf1b9d22d19f5e9b3', ['class' => 'btn btn-default']) ?>
                        </p>
                        <hr>
                        <h4>Вопрос 3</h4>
                        <p>Вам нужно строку "Купи слона" преобразовать в "Kupi slona". Приложите ссылку на код.</p>
                        <p>
                            <?= Html::a('Demo', ['/test/slug'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Source', 'https://bitbucket.org/pravda1979/test-project/commits/717fb81c0ea0a701ec1ba1ec3245fd64af358fa6', ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Эссе</h3>
                    </div>
                    <div class="panel-body">
                        <p>Просмотреть видео и написать краткое эссе по каждому. Ссылку на архив приложить.</p>
                        <p>
                            <?= Html::a('Читать', ['/test/essay'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Скачать', ['/files/essay.zip'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">TinyMCE</h3>
                    </div>
                    <div class="panel-body">
                        <p>Написать компонент визуального редактора ( на выбор ) , с возможностью его настройки и
                            установки через composer. Зарегистрировать на https://packagist.org. Приложить ссылку.</p>
                        <p>
                            <?= Html::a('Demo', ['/test/tinymce'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('GitHub', 'https://github.com/pravda1979/yii2-tinymce', ['class' => 'btn btn-default']) ?>
                            <?= Html::a('Packagist', 'https://packagist.org/packages/pravda1979/yii2-tinymce', ['class' => 'btn btn-success']) ?>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
