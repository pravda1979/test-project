<?php

namespace app\components;

/**
 * @inheritdoc
 *
 * Добавлена функция для форматирования телефонных номеров, хранящихся в числовом формате
 * Для использования нужно сконфигурировать компонент приложения formatter:
 * 'formatter' => [
 *     'class' => 'app/components/Formatter', // Путь к классу
 * ]
 * Использование: Yii::$app->formatter->asPhone($phoneNumber)
 * или Yii::$app->formatter->asPhone($phoneNumber, $phoneFormat) если нужно изменить формат отображения едидноразово.
 * Если нужно изменить формат отображения для всего приложения и/или регулярное выражение под формат,
 * нужно сконфигурировать компонент приложения formatter следующим образом:
 * 'formatter' => [
 *     'class' => 'app/components/Formatter',                   // Путь к классу
 *     'phoneRegexp' = '/^(\d)(\d{3})(\d{3})(\d{2})(\d{2})$/',  // Регулярное выражение
 *     'phoneFormat' => '+$1($2)$3-$4-$5'                       // Формат отображения
 * ]
 */

class Formatter extends \yii\i18n\Formatter
{
    /**
     * @var string Регулярное выражение для числового знаяения телефонного номера
     * По умолчанию значение равно '/^(\d)(\d{3})(\d{3})(\d{2})(\d{2})$/'
     */
    public $phoneRegexp = '/^(\d)(\d{3})(\d{3})(\d{2})(\d{2})$/';

    /**
     * @var string Формат отображения телефонного номера
     * По умолчанию значение равно '+$1($2)$3-$4-$5'
     */
    public $phoneFormat = '+$1($2)$3-$4-$5';

    /**
     * Возвращает форматированное значение телефонного номера, хранящегося в числовом виде.
     * По умолчанию возвращает значение в виде +9(999)999-99-99.
     *
     * @param $value string Значение телефонного номера, только цифры
     * @param null $format Формат телефонного номера
     * @return string
     */
    public function asPhone($value, $format = null)
    {
        return preg_replace($this->phoneRegexp, $format ?? $this->phoneFormat, $value);
    }
}